package com.example.andrea.myapp;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.ProgressBar;


import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import static com.example.andrea.myapp.RecognitionActivity.Hue;



public class CameraActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {
    public static String tipo_figura, figura_definitiva;
    public static int contador_triangulo, contador_cuadrado, contador_rectangulo, contador_circulo, contador_hexagono=0;
    public static SequenceActivity seq = new SequenceActivity();
    public static ProgressBar simpleProgressBar;
    public static String color = "";


    private Mat mRgba; //frame (se va actualizando todoel tiempo)
    private static final String TAG = "OCVSample::Activity";
    private CameraBridgeViewBase mOpenCvCameraView;
    public static String SECUENCIA;


    static {
        if (!OpenCVLoader.initDebug()) {
            Log.i("opencv", "inicializacion fallida");
        } else
            Log.i("opencv", "inicializacion correcta");
    }


    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    public CameraActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_camera);
        



        simpleProgressBar = (ProgressBar) findViewById(R.id.progressBar); // initiate the progress bar




        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.OpenCvView);
            mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
            mOpenCvCameraView.setCvCameraViewListener(this);//llama a OnCameraFrame
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

       if(savedInstanceState==null) { //Para que solo ejecute "cargarSecuencia" una vez (por alguna razón se ejecuta onCreate dos veces)
            //Get the clicked sequence
            Intent intent = getIntent();
            SECUENCIA = intent.getStringExtra("secuencia");
            seq.cargarSecuencia(SECUENCIA);
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {

        mRgba = new Mat(height, width, CvType.CV_8UC4);

    }

    public void onCameraViewStopped() {

        mRgba.release();
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {


        mRgba = inputFrame.rgba();
        Mat mRgba_copy = mRgba.clone();

        //Dibujar rectángulo tarjeta
        int rec_side = 500;
        int x_rec = (mRgba.cols() / 2) - rec_side / 2;
        int y_rec = (mRgba.rows() / 2) - 700 / 2;
        Rect tarjeta = new Rect(x_rec, y_rec, rec_side, 700);
        Imgproc.rectangle(mRgba, new Point(tarjeta.x, tarjeta.y), new Point(tarjeta.x + tarjeta.width, tarjeta.y + tarjeta.height), new Scalar(240, 240, 240, 255), 5);

        //Máscara inversa


        //Dibujar cuadrado ROI
        int square_side = 200;
        int x = (mRgba.cols() / 2) - square_side / 2;
        int y = (mRgba.rows() / 2) + square_side / 2;
        Rect roi = new Rect(x, y, square_side, square_side);
        Imgproc.rectangle(mRgba, new Point(roi.x, roi.y), new Point(roi.x + roi.width, roi.y + roi.height), new Scalar(240, 240, 240, 255), 5);

        //Recortamos la imagen
        Mat matROI = new Mat(mRgba_copy, roi);


        //Encontrar los contornos de la imagen recortada
        RecognitionActivity recognition = new RecognitionActivity();

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        contours = recognition.findContours(matROI, contours);

        //Detectamos la figura
        tipo_figura = recognition.figureClasification(contours);


        switch (tipo_figura) {
            case"TRIANGULO":
                contador_triangulo++;
                break;
            case "CUADRADO":
                contador_cuadrado++;
                break;
            case "CIRCULO":
                contador_circulo++;
                break;
            case "RECTANGULO":
                contador_rectangulo++;
                break;
            case "HEXAGONO":
                contador_hexagono++;
            default:
                break;
        }

        if (contador_triangulo >= 30) {

            //Detectamos el color
            color = recognition.colorDetection(matROI);
           // seq.comprobarColor(color);
            Imgproc.putText(mRgba, "TRIANGULO " + color + Hue, new Point(100, 100), 3, 1, new Scalar(255, 0, 0, 255), 2);
            if (contador_triangulo == 30) {
                figura_definitiva = "TRIANGULO";
                seq.comparar(figura_definitiva);


            }


                if (contador_triangulo >= 40) { //ya lo ha detectado
                    resetCounters();
                    esperar(1);
                }

        }

        if (contador_hexagono >= 30) {
            color = recognition.colorDetection(matROI);
          //  seq.comprobarColor(color);
            Imgproc.putText(mRgba, "HEXAGONO "+color+Hue, new Point(100, 100), 3, 1, new Scalar(255, 0, 0, 255), 2);
            if (contador_hexagono == 30) {
                figura_definitiva = "HEXAGONO";
                seq.comparar(figura_definitiva);


            }


            if (contador_hexagono >= 40) { //ya lo ha detectado
                resetCounters();
                esperar(1);

            }
        }

        if (contador_cuadrado >= 30) {
            color = recognition.colorDetection(matROI);
          //  seq.comprobarColor(color);
            Imgproc.putText(mRgba, "CUADRADO "+color+Hue, new Point(100, 100), 3, 1, new Scalar(255, 0, 0, 255), 2);
            if (contador_cuadrado == 30) {
                figura_definitiva = "CUADRADO";
                seq.comparar(figura_definitiva);

            }

            if (contador_cuadrado >= 40) { //ya lo ha detectado

                resetCounters();
                esperar(1);
            }
        }

        if (contador_rectangulo >= 30) {
            color = recognition.colorDetection(matROI);
         //   seq.comprobarColor(color);
            Imgproc.putText(mRgba, "RECTANGULO "+color+Hue, new Point(100, 100), 3, 1, new Scalar(255, 0, 0, 255), 2);
            if (contador_rectangulo == 30) {
                figura_definitiva = "RECTANGULO";
                seq.comparar(figura_definitiva);


            }

            if (contador_rectangulo >= 40) { //ya lo ha detectado

                resetCounters();
                esperar(1);
            }
        }

        if (contador_circulo >= 30) {
            color = recognition.colorDetection(matROI);
      //      seq.comprobarColor(color);
            Imgproc.putText(mRgba, "CIRCULO "+color+Hue, new Point(100, 100), 3, 1, new Scalar(255, 0, 0, 255), 2);
            if (contador_circulo == 30){
                figura_definitiva = "CIRCULO";
                seq.comparar(figura_definitiva);


            }
            if (contador_circulo == 40) { //ya lo ha detectado

            resetCounters();
            esperar(1);
        }
    }


       // Imgproc.drawContours(mRgba, contours, -1, new Scalar(255, 0, 0, 255), 2);

       // Imgproc.putText(mRgba, resultado, new Point(300, 300), 3, 1, new Scalar(255, 255, 0, 255), 2);


if(SequenceActivity.resultado=="end"){ //resultado es static

    FragmentManager fm = getFragmentManager();
    MyDialogFragment dialogFragment = new MyDialogFragment();
    dialogFragment.setStyle(DialogFragment.STYLE_NORMAL,R.style.Dialog);
    dialogFragment.show(fm,"Fragment Dialog");
    onPause();


}else if(SequenceActivity.resultado=="fail") { //resultado es static

    FragmentManager fm = getFragmentManager();
    MyDialogFragment dialogFragment = new MyDialogFragment();
    dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.Dialog);
    dialogFragment.show(fm, "Fragment Dialog");
    onPause();

}
     return mRgba; //lo que se ve
    }


    public void resetCounters () {

        contador_triangulo = 0;
        contador_cuadrado = 0;
        contador_rectangulo = 0;
        contador_circulo = 0;
        contador_hexagono = 0;
    }


    public void esperar (int segundos) {
        try {
            Thread.sleep(segundos * 1000);
        } catch (Exception e) {
// Mensaje en caso de que falle
        }
    }



}