package com.example.andrea.myapp;

import android.os.Bundle;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Andrea on 27/01/2018.
 */

public class MyDialogFragment extends DialogFragment {



    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

       View rootView = null;

        if (SequenceActivity.resultado == "end") {
            rootView = inflater.inflate(R.layout.fragment_dialog, container, false);
            getDialog().setTitle("¡Enhorabuena!");

        }

       else if (SequenceActivity.resultado == "fail") {
            rootView = inflater.inflate(R.layout.fragmentfail_dialog, container, false);
            getDialog().setTitle("¡Vaya!");

        }


        return rootView;
    }




}
