package com.example.andrea.myapp;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
//import static com.example.andrea.myapp.SequenceActivity;



public class Welcome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

     /*   if (SequenceActivity.resultado == "end"){
            Log.i("opencv", "HA ENTRADO EN EL LOOP");
            AlertDialog.Builder alert1 = new AlertDialog.Builder(Welcome.this);
            alert1.setMessage("Has ordenado la secuencia correctamente.");
            alert1.setTitle("Enhorabuena!");
            AlertDialog dialog = alert1.create();
            dialog.show();
        }*/

            /*AlertDialog.Builder alert1 = new AlertDialog.Builder(Welcome.this);
            alert1.setMessage("Has ordenado la secuencia correctamente.");
            alert1.setTitle("Enhorabuena!");
            AlertDialog dialog = alert1.create();
            dialog.show();*/




    }

    public void goToSubmenu(View v){
        Button button = (Button)v;
        String buttonText = button.getText().toString();

        //equals compara dos cadenas tal cuál,no sus instancias
        // (se pueden comparar cadenas de texto literales sin necesitar una variable)
       if(buttonText.equals("Fácil")){
            setContentView(R.layout.submenu_facil);
        }

        else if(buttonText.equals("Medio")){
            setContentView(R.layout.submenu_medio);
        }

        else if(buttonText.equals("Difícil")){
            setContentView(R.layout.submenu_dificil);
        }


    }

    public void goToCam(View v){
        Button b = (Button)v;
        String description = b.getContentDescription().toString();
        Intent intent = new Intent(this, CameraActivity.class);
        intent.putExtra("secuencia", description);
        startActivity(intent);

    }

    public void goToInfo(View v){

        setContentView(R.layout.help_text);
    }

    public void backToMenu(View v){
        setContentView(R.layout.activity_welcome);


    }



}
